package ukdw.ac.id.tugas4;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapsActivity extends Activity implements LocationListener {

    private GoogleMap googleMap;
    LocationManager lm;
    Location location;
    Double latitudeC;
    Double longitudeC;
    MarkerOptions marker2;
    LatLng latlng;

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    public static final String PREFS_NAME = "MyPrefsFile";
    static SharedPreferences settings;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        settings = getSharedPreferences(PREFS_NAME, 0);
        editor = settings.edit();

        latitudeC = Double.parseDouble(settings.getString("latitude","0.0"));
        longitudeC = Double.parseDouble(settings.getString("longitude","0.0"));

        Toast.makeText(getApplicationContext(), settings.getString("latitude","0.0") + "," + settings.getString("longitude","0.0") , Toast.LENGTH_LONG).show();

        try {
            //Loading map
            initializeMap();
        }
        catch (Exception e){
            e.printStackTrace();
        }

//        sampai sini dulu, besok diedit lagi;

        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        //String provider = lm.getBestProvider(new Criteria(), true);
        //location = lm.getLastKnownLocation(provider);

        location = new Location("");
        location.setLatitude(latitudeC);
        location.setLongitude(longitudeC);

        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME,MIN_DISTANCE,this);
        if (isLocation()==true){
            onLocationChanged(location);
            //onLocationChanged(lm.isProviderEnabled(LocationManager.GPS_PROVIDER));
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("to use this feature enable location");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),1);
                    dialog.dismiss();
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeC = location.getLatitude();
        longitudeC = location.getLongitude();
        latlng = new LatLng(latitudeC, longitudeC);

        if (marker2 == null){
            marker2 = new MarkerOptions().position(new LatLng(latitudeC, longitudeC)).title(""+"my location").snippet("posisi saya di lat : "+latitudeC+" long : "+longitudeC).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            googleMap.addMarker(marker2);

            //when the location changes, update the map by zooming to the location
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
            googleMap.animateCamera(cameraUpdate);
        }
        else{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void initializeMap() {
        if (googleMap == null){
            googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
            if (googleMap == null){
                Toast.makeText(getApplicationContext(), "Sorry! Unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean isLocation(){
        boolean enabled;
        enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return enabled;
    }

    @Override
    protected void onResume(){
        super.onResume();
        initializeMap();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == 1){
            if (isLocation()==true){
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
    }
}
