//
//  main.m
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
