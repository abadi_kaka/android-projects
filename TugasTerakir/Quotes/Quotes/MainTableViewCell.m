//
//  MainTableViewCell.m
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import "MainTableViewCell.h"
#import "AFHTTPRequestOperationManager.h"

@implementation MainTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btn_delete:(id)sender {
    id quoteId = _lbl_id.text;
    NSString *url = [NSString stringWithFormat:@"http://lecturer.ukdw.ac.id/yuan/labs/quotes2/%@", quoteId];
    
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request DELETE:url parameters:quoteId success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Delete id %@ berhasil!", quoteId);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Delete id %@ gagal!", quoteId);
    }];
}

@end
