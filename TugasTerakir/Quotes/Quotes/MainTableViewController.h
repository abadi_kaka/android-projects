//
//  MainTableViewController.h
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewController : UITableViewController {
    // array of dictionary
    NSArray *quotes;
    
    // untuk tambah fitur pull to refresh
    UIRefreshControl *pullToRefresh;
    
    NSString *send_id;
    NSDictionary *send_dict_quote;
}
- (IBAction)btn_refresh:(id)sender;
- (IBAction)btn_add:(id)sender;
- (IBAction)btn_edit:(id)sender;

@end
