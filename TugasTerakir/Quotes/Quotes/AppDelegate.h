//
//  AppDelegate.h
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
