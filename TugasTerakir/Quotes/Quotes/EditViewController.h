//
//  EditViewController.h
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/9/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textAuthor;
@property (weak, nonatomic) IBOutlet UITextField *textQuote;
- (IBAction)btn_edit:(id)sender;

@property (nonatomic) NSDictionary *get_dict_quote;

@end
