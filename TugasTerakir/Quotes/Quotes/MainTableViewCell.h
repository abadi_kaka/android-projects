//
//  MainTableViewCell.h
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_id;
@property (weak, nonatomic) IBOutlet UILabel *lbl_quote;
@property (weak, nonatomic) IBOutlet UILabel *lbl_author;
- (IBAction)btn_delete:(id)sender;

@end
