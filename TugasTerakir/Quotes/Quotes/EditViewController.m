//
//  EditViewController.m
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/9/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import "EditViewController.h"
#import "AFHTTPRequestOperationManager.h"

@interface EditViewController ()

@end

@implementation EditViewController
@synthesize get_dict_quote;
id quoteId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _textAuthor.text = [get_dict_quote objectForKey:@"author"];
    _textQuote.text = [get_dict_quote objectForKey:@"quote"];
    quoteId = [get_dict_quote objectForKey:@"id"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_edit:(id)sender {
    NSString *url = @"http://lecturer.ukdw.ac.id/yuan/labs/quotes2/";
    
    NSString *author = _textAuthor.text;
    NSString *quote = _textQuote.text;
    
    NSDictionary *new_dict_quote = [NSDictionary dictionaryWithObjectsAndKeys:author, @"author", quote, @"quote", quoteId, @"id", nil];
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request PUT:url parameters:new_dict_quote success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Edit berhasil!");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Edit gagal!");
    }];
}
@end
