//
//  AddViewController.m
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/9/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import "AddViewController.h"
#import "AFHTTPRequestOperationManager.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_add:(id)sender {
    // but latihan, coba buat function untuk add quotes
    // siapkan url yg akan direquest
    NSString *url = @"http://lecturer.ukdw.ac.id/yuan/labs/quotes2/";
    
    // ambil text inputan
    NSString *inputAuthor = _textTheAuthor.text;
    NSString *inputQuote = _textNewQuote.text;
    
    // siapkan parameter (author,quote)
    NSDictionary *pm = [[NSDictionary alloc] initWithObjectsAndKeys:inputQuote, @"quote", inputAuthor, @"author", nil];
    
    // afnetworking get
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    
    [request POST:url parameters:pm success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"POST sukses!");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"POST gagal!");
    }];

}
@end
