//
//  AddViewController.h
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/9/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textTheAuthor;
@property (weak, nonatomic) IBOutlet UITextField *textNewQuote;
- (IBAction)btn_add:(id)sender;

@end
