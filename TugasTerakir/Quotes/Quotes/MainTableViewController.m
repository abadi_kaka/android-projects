//
//  MainTableViewController.m
//  Quotes
//
//  Created by Hizkia Juan Suryanto on 12/5/14.
//  Copyright (c) 2014 ios.ti.ukdw.ac.id. All rights reserved.
//

#import "MainTableViewController.h"
#import "AFNetworking.h"
#import "MainTableViewCell.h"
#import "AddViewController.h"
#import "EditViewController.h"

@interface MainTableViewController ()

@end

@implementation MainTableViewController

// pelajari method lainnya : https://github.com/AFNetworking/AFNetworking#get-request
// apps untuk jalananin method REST : https://luckymarmot.com/paw (on Mac)
// RESTFire on Windows

// request GET quotes akan dilakukan 3 x --> saat view didload, refresh, dan pull to refresh
// so, make a function for it
-(void)refreshQuotes {
    // siapkan url yg akan direquest
    NSString *url = @"http://lecturer.ukdw.ac.id/yuan/labs/quotes2/all";
    
    // afnetworking get
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    [request GET:url parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             // dilakukan kalau request berhasil
             NSLog(@"Request success! Result : %@", responseObject);
             
             // array of objects
             quotes = [[NSArray alloc] initWithArray:responseObject];
             
             // reload table view
             [self.tableView reloadData];
             
             // hilangkan pullToRefresh (refresh Control)
             [pullToRefresh endRefreshing];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // dilakukan kalau request gagal
             NSLog(@"Request failed! Reason : %@", error);
             
             // hilangkan pullToRefresh (refresh Control)
             [pullToRefresh endRefreshing];
         }
    ];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    // lakukan hanya SATU kali , saat viewdidload
    pullToRefresh = [[UIRefreshControl alloc] init];
    [pullToRefresh addTarget:self action:@selector(refreshQuotes) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:pullToRefresh];
    // sampai ini, refresh icon muncul, tapi blum mau balik.
    
    
    // web service pake punya p.yuan : http://lecturer.ukdw.ac.id/yuan/labs/quotes2/readme
    // kirim request GET ke quotes web services
    [self refreshQuotes];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self refreshQuotes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return quotes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Get the data...
    NSDictionary *dict_quote = [quotes objectAtIndex:indexPath.row];
    
    // Configure the cell...
    cell.lbl_id.text = [dict_quote objectForKey:@"id"];
    cell.lbl_author.text = [dict_quote objectForKey:@"author"];
    cell.lbl_quote.text = [dict_quote objectForKey:@"quote"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict_quote = [quotes objectAtIndex:indexPath.row];
    send_id = [dict_quote objectForKey:@"id"];
    send_dict_quote = dict_quote;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // tambah code ini
        
        NSDictionary *dict_quotes = [quotes objectAtIndex:indexPath.row];
        NSString *getId = [dict_quotes objectForKey:@"no_id"];
        
        NSString *url = @"http://lecturer.ukdw.ac.id/yuan/labs/quotes2/";
        
        AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
        [request DELETE:url parameters:getId success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Delete id %@ berhasil!", getId);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Delete id %@ gagal!", getId);
        }];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"showAddQuote"]) {
       AddViewController *avc = segue.destinationViewController;
    }
    else {
        EditViewController *evc = segue.destinationViewController;
        evc.get_dict_quote = send_dict_quote;
    }
    
}


- (IBAction)btn_refresh:(id)sender {
    [self refreshQuotes];
}

- (IBAction)btn_add:(id)sender {
//    [self performSegueWithIdentifier:@"showAddQuote" sender:self];
}

- (IBAction)btn_edit:(id)sender {
//    EditViewController *evc;
//    evc.get_id = send_id;
//    evc.get_dict_quote = send_dict_quote;
}

@end
