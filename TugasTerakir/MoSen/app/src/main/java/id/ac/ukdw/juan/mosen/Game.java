package id.ac.ukdw.juan.mosen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class Game extends Activity implements SensorEventListener{
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float deltaX = 0;
    private float deltaY = 0;
    private ImageView mosen,enemy;
    private float x,y;
    TextView textViewScore,textViewTimer;
    int timer = 10;
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initializeViews();

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer,SensorManager.SENSOR_DELAY_GAME);

            enemy.setVisibility(View.VISIBLE);
            setScore();
            setEnemy();
            startTimer();
        }
        else {
            // fail! we dont have an accelerometer!
            Toast.makeText(Game.this, "Oops! We don't have an accelerometer!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initializeViews() {
        mosen = (ImageView)findViewById(R.id.mosen);
        enemy = (ImageView)findViewById(R.id.enemy);
        textViewScore = (TextView)findViewById(R.id.score);
        textViewTimer = (TextView)findViewById(R.id.timer);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        deltaX = event.values[0];
        deltaY = event.values[1];

        // set coordinate of MoSen
        x = mosen.getX();
        x-=deltaX;
        y = mosen.getY();
        y+=deltaY;
        setCoordinate(x,y);

        // if mosen touch the enemy
        if(mosen.getX() <= enemy.getX()+5 ||
                mosen.getY() <= enemy.getY()+5) {
            score+=10;
            setScore();
            enemy.setVisibility(View.GONE);
            timer = 10;
            startTimer();
            setEnemy();
        }
    }

    public void setCoordinate(float x, float y) {
        // if mosen hit the edge
        mosen.setX(x);
        mosen.setY(y);
    }

    public void startTimer() {
//        timer--;
        new CountDownTimer(10000,1000) {
            public void onTick(long m) {
                textViewTimer.setText("Time : " + --timer + " s");
            }

            public void onFinish() {
                onPause();
                Toast.makeText(getApplicationContext(),"GAME OVER\nYour score : " + score, Toast.LENGTH_LONG).show();
    //                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        }.start();
    }

    public void setEnemy() {
        // set new position of enemy
        Random random = new Random();
        enemy.setX(random.nextInt(450)+1);
        random = new Random();
        enemy.setY(random.nextInt(450)+1);
        enemy.setVisibility(View.VISIBLE);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    public void setScore() {
        textViewScore.setText("Score : " + score + " points");
    }
}
