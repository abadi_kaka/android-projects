package com.securevault.venom.abadiwinnie;

import android.widget.ImageView;

import java.util.Random;

public class Objek {
    private static final Random random = new Random();
    private int x;
    private int y;
    private int score=0;
    private float rotation=0;
    private int radius;
    private ImageView imageView;



    public void setPosition(float x,float y){
        setPosition((int) x, (int) y);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void syncImg(){
        imageView.setX(this.x+this.radius);
        imageView.setY(this.y+this.radius);
        imageView.setRotation(this.rotation);
    }
    public boolean isCollidingWith(Objek objectCircle){
        float dx = objectCircle.getX()-this.x;
        float dy = objectCircle.getY()-this.y;
        double distance = Math.sqrt((dx*dx)+(dy*dy));
        if(distance<(this.radius+objectCircle.getRadius())-5){
            return true;
        }else{
            return false;
        }

    }
    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setPosition(int x,int y){
        this.x=x;
        this.y=y;
        syncImg();
    }
    public void newPosition(int width, int height){
        setPosition(random.nextInt(width),random.nextInt(height));
    }
    public Objek(ImageView imageView,int radius,int width,int height,boolean bol){
        this.imageView=imageView;
        this.radius=radius;
        newPosition(width,height);
    }
    public Objek(ImageView imageView, int radius, int x, int y) {
        this.imageView = imageView;
        this.radius = radius;
        this.x = x;
        this.y = y;
        syncImg();
    }
    public Objek(ImageView imageView,int x,int y){
        newCircle(imageView,x,y);
    }
    public Objek(ImageView imageView,float x,float y){
        newCircle(imageView,(int)x,(int)y);
    }
    private void newCircle(ImageView imageView,int x,int y){
        this.imageView =imageView;
        this.x=x;
        this.y=y;
        this.radius=imageView.getWidth()/2;
        syncImg();
    }
    public int getScore(){
        return this.score;
    }
    public void addScore(int value){
        this.score+=value;
    }
    public String scoreToString(){
        return new StringBuilder().append(score).toString();
    }
}
