package com.securevault.venom.abadiwinnie;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private ImageView player,obstacle1,obstacle2;
    private SensorManager sensorManager;
    private Sensor accSensor;
    private Objek objPlayer,objObs1,objObs2,objObs3;
    private DisplayMetrics displayMetrics;
    private ArrayList<Objek> listObstacle = new ArrayList<>();
    private int width,height;
    private TextView score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)!=null){
            accSensor=sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this,accSensor,SensorManager.SENSOR_DELAY_GAME);
        }
        displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        width=displayMetrics.widthPixels;
        height=displayMetrics.heightPixels;
        score = (TextView) findViewById(R.id.txtScore);
        player=(ImageView)findViewById(R.id.player);
        obstacle1 =(ImageView)findViewById(R.id.obstacle1);
        obstacle2 =(ImageView)findViewById(R.id.obstacle2);
        objPlayer = new Objek(player,player.getWidth()/2,width/2,height/2);
        objObs1 = new Objek(obstacle1,obstacle1.getWidth()/2,width-50,height-50,true);
        objObs2 = new Objek(obstacle2,obstacle2.getWidth()/2,width-100,height-100,true);
        listObstacle.add(objObs1);
        listObstacle.add(objObs2);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        objPlayer.setRadius(player.getWidth()/2);
        objObs1.setRadius(obstacle1.getWidth()/2);
        objObs2.setRadius(obstacle2.getWidth()/2);
       objPlayer.setPosition(objPlayer.getX() - event.values[0], objPlayer.getY() + event.values[1]);
        if(objPlayer.getX()>width&&event.values[0]<0){
            objPlayer.setX(-30);
        }
        if(objPlayer.getX()<=-60&&event.values[0]>0){
            objPlayer.setX(width + 30);
        }
        if(objPlayer.getY()<-60&&event.values[1]<0){
            objPlayer.setY(height + 30);
        }
        if(objPlayer.getY()>height&&event.values[1]>0){
            objPlayer.setY(-30);
        }
        for (int i=0;i<listObstacle.size();i++){
            if(objPlayer.isCollidingWith(listObstacle.get(i))){
                listObstacle.get(i).newPosition(width-100,height-100);
                objPlayer.addScore(1);
                score.setText("SKOR PERMAINAN "+objPlayer.scoreToString());
            }
        }

    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //donothing
    }
}
