package id.ac.ukdw.tugas1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

/**
 * Created by macbook on 3/15/15.
 */
public class Text extends Activity implements OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);
        Button btnToast = (Button)findViewById(R.id.button);

        btnToast.setOnClickListener(this);
    }
    public void onClick(View v) {
        EditText edit = (EditText)findViewById(R.id.editText);

        Toast toast = toastFromEditText(edit);
        toast.show();
    }
    private Toast toastFromEditText(EditText edit) {
        // TODO Auto-generated method stub
        return Toast.makeText(this, edit.getText().toString(), Toast.LENGTH_LONG);
    }
}
