package id.ac.ukdw.tugas1;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by macbook on 3/15/15.
 */
public class Dial extends Activity implements View.OnClickListener{

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dial);
        Button btnCall = (Button)findViewById(R.id.button2);
        btnCall.setOnClickListener(this);
    }
    public void onClick(View v) {
        call();
    }
    private void call() {
        try {
            Intent phoneIntent = new Intent(Intent.ACTION_CALL);
            phoneIntent.setData(Uri.parse("tel:085-747-519-850"));
            startActivity(phoneIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("helloandroid dialing example", "Call failed", activityException);
        }
    }


}
