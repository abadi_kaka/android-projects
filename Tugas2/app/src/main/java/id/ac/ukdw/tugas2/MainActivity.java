package id.ac.ukdw.tugas2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private DBHelper mydb;
    TextView username;
    TextView password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void signInClick(View v){
        username = (TextView)findViewById(R.id.editText);
        password = (TextView)findViewById(R.id.editText2);
        mydb = new DBHelper(this);
        Username user = mydb.findUsername(username.getText().toString(),password.getText().toString());
        if(user != null){
            Bundle dataBundle = new Bundle();
            dataBundle.putInt("id", user.getId());
            dataBundle.putString("fullname", user.getName());
            dataBundle.putString("username", user.getUsername());
            dataBundle.putString("foto", user.getFoto());
            dataBundle.putString("password", user.getPassword());
            Intent intent = new Intent( getApplicationContext(),
                    DisplayUser.class
            );
            intent.putExtras(dataBundle);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(), "Username or password wrong!!!!!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void signUpClick(View v){
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
