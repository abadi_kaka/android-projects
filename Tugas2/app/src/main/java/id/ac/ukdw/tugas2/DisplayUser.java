package id.ac.ukdw.tugas2;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarActivity; import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by macbook on 4/12/15.
 */
public class DisplayUser extends ActionBarActivity{
    private DBHelper mydb;
    TextView fullname;
    TextView username;
    TextView password;
    ImageView foto;
    String path1;
    int id_To_Update;
    Uri uri = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
        fullname = (TextView) findViewById(R.id.EditFullname);
        username = (TextView) findViewById(R.id.EditUsername);
        password = (TextView) findViewById(R.id.editText6);
        foto = (ImageView) findViewById(R.id.imageView);

        mydb = new DBHelper(this);
        Bundle extras = getIntent().getExtras();
        if(extras != null ){

            id_To_Update = extras.getInt("id");
            if (!extras.getBoolean("write")){

                fullname.setText(extras.getString("fullname"));
                username.setText(extras.getString("username"));
                password.setText(extras.getString("password"));
                path1 = extras.getString("foto");
                uri = Uri.parse(extras.getString("foto"));
                foto.setImageURI(uri);

            }
        }
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

    }

    public void onClickOut(View v){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void onEdited(View v) {
        mydb = new DBHelper(this);
        mydb.updateProfil(id_To_Update,
        fullname.getText().toString(), username.getText().toString(),password.getText().toString(), path1);
        Toast.makeText(getApplicationContext(), "Updated",
        Toast.LENGTH_SHORT).show();

    }
    public void selectImage(){
        final CharSequence[] options = { "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayUser.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 1);

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView foto = (ImageView)findViewById(R.id.imageView);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
           if (requestCode == 1) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                path1 = selectedImage.toString();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("path of image from gallery......******************.........", picturePath + "");
                foto.setImageBitmap(thumbnail);
            }
        }
    }

}
