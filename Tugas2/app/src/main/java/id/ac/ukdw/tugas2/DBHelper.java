package id.ac.ukdw.tugas2;

/**
 * Created by macbook on 4/12/15.
 */
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "MyDatabase.db";
    public static final String PROFIL_TABLE_NAME = "profil";
    public static final String PROFIL_COLUMN_ID = "id";
    public static final String PROFIL_COLUMN_FULLNAME = "fullname";
    public static final String PROFIL_COLUMN_USERNAME = "username";
    public static final String PROFIL_COLUMN_PASSWORD = "password";
    public static final String PROFIL_COLUMN_PHOTO = "foto";

    // membuat constructor yang memanggil superclass untuk membuat databse.
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE profil " +
                "(id INTEGER PRIMARY KEY, fullname TEXT, username TEXT, password TEXT, foto TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS profil");
        onCreate(db);
    }

    public boolean insertProfil(String fullname, String username, String password, String foto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
// data yang akan dimasukan kedalam tabel
        contentValues.put("fullname", fullname);
        contentValues.put("username", username);
        contentValues.put("password", password);
        contentValues.put("foto", foto);

// memasukan data kedalam tabel
// selain menggunakan ContentValue, ada cara lain untuk // INSERT data ke dalam database.
// Yaitu dengan memasukan syntax secara manual.
        db.insert("profil", null, contentValues);
        return true;
    }

   public boolean updateProfil(Integer id, String fullname, String username, String password, String text) {
       SQLiteDatabase db = this.getWritableDatabase();
// contoh UPDATE database dengan SYNTAX.
       db.execSQL("UPDATE profil SET fullname = '" + fullname + "',username = '" + username +
               "',password = '" + password + "',foto = '" + text + "' WHERE id = '" + id + "'");
       return true;
   }


    public boolean updateProfil1(Integer id, String fullname, String username, String foto) {
        SQLiteDatabase db = this.getWritableDatabase();
// contoh UPDATE database dengan SYNTAX.
        db.execSQL("UPDATE profil SET fullname = '" + fullname + "',username = '" + username + "',foto = '" + foto +"' WHERE id = '" + id + "'");
        return true;
    }

    public Integer deleteContact(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM profil WHERE id = '" + id + "'");
        return id;
    }

    public Username findUsername(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "Select * FROM profil WHERE username = '" + username + "' AND password = '" + password + "'";

        Cursor cursor = db.rawQuery(query, null);

        Username user = new Username();
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            user.setId(cursor.getInt(cursor.getColumnIndex(PROFIL_COLUMN_ID )));
            user.setName(cursor.getString(cursor.getColumnIndex(PROFIL_COLUMN_FULLNAME )));
            user.setUsername(cursor.getString(cursor.getColumnIndex(PROFIL_COLUMN_USERNAME )));
            user.setPassword(cursor.getString(cursor.getColumnIndex(PROFIL_COLUMN_PASSWORD )));
            user.setFoto(cursor.getString(cursor.getColumnIndex(PROFIL_COLUMN_PHOTO )));
            cursor.close();
        } else {
            user = null;
        }
        db.close();
        return user;
    }

}