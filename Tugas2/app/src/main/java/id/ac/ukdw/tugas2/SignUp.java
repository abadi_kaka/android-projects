package id.ac.ukdw.tugas2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by macbook on 4/13/15.
 */
public class SignUp extends ActionBarActivity{
    private DBHelper mydb;

    String path1 ="duhdek";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        ImageView foto = (ImageView)findViewById(R.id.imageView2);

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    public void onClick(View v){
        TextView fullname = (TextView)findViewById(R.id.editText5);
        TextView username = (TextView)findViewById(R.id.editText3);
        TextView password = (TextView)findViewById(R.id.editText4);
        TextView retype = (TextView)findViewById(R.id.editText8);

        if(retype.getText().toString().equals(password.getText().toString()))
        {
            mydb = new DBHelper(this);
            mydb.insertProfil(fullname.getText().toString(),username.getText().toString(),password.getText().toString(), path1);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        else
        {
            Toast.makeText(getApplicationContext(), "Password and retype is different!!!!!!",
                    Toast.LENGTH_SHORT).show();
        }
        //ImageView foto = (ImageView)findViewById(R.id.imageView2);

    }


    public void selectImage(){
        final CharSequence[] options = { "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
        public void onClick(DialogInterface dialog, int item) {
           if (options[item].equals("Choose from Gallery"))
            {
                Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);

            }
            else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        }
    });
    builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView foto = (ImageView)findViewById(R.id.imageView2);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                path1 = selectedImage.toString();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.w("path of image from gallery......******************.........", picturePath + "");
                foto.setImageBitmap(thumbnail);
            }
        }
    }
}
