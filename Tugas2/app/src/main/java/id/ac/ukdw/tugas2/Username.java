package id.ac.ukdw.tugas2;

/**
 * Created by macbook on 4/12/15.
 */
public class Username {
    int id;
    String fullname, username,password,foto;

    public int getId() {
        return id; }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName() {
        return fullname; }
    public void setName(String fullname) { this.fullname = fullname;
    }
    public String getUsername() {
        return username; }
    public void setUsername(String username) { this.username = username;
    }


    public String getPassword() {
        return password; }
    public void setPassword(String password) { this.password = password;
    }
    public String getFoto() {
        return foto; }
    public void setFoto(String foto) { this.foto = foto;
    }
    @Override
    public String toString(){ return this.fullname;
    }
}
