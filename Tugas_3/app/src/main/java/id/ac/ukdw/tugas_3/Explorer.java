package id.ac.ukdw.tugas_3;

/**
 * Created by macbook on 4/28/15.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Explorer extends Activity {
    ListView listView;
    Button butt, buttDir;
    TextView text;
    private String path;
    private String sourceImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_explorer);
        listView = (ListView)findViewById(R.id.listShow);
        butt = (Button)findViewById(R.id.button);
        buttDir = (Button)findViewById(R.id.chooseDir);
        text = (TextView)findViewById(R.id.textView);

        path = Environment.getExternalStorageDirectory().toString();
        if (getIntent().hasExtra("path")) {
            path = getIntent().getStringExtra("path");
        }
        text.setText(path);

        if (getIntent().hasExtra("sourceImage")) {
            sourceImage = getIntent().getStringExtra("sourceImage");
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String filename = (String) listView.getAdapter().getItem(position);
                if (path.endsWith(File.separator)) {
                    filename = path + filename;
                } else {
                    filename = path + File.separator + filename;
                }

                if (new File(filename).isDirectory()) {
                    Intent intent = new Intent(Explorer.this, Explorer.class);
                    intent.putExtra("path", filename);
                    intent.putExtra("sourceImage",sourceImage);
                    startActivity(intent);
                } else {
                    Toast.makeText(Explorer.this, filename + " is not a directory", Toast.LENGTH_LONG).show();
                }
            }
        });

        buttDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Explorer.this,Proses.class);
                in.putExtra("newDir",path);
                in.putExtra("sourceImage",sourceImage);
                startActivity(in);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        List values = new ArrayList();
        File dir = new File(path);
        if (!dir.canRead()) {
            setTitle(getTitle() + " (inaccessible)");
        }
        String[] list = dir.list();
        if (list != null) {
            for (String file : list) {
                if (!file.startsWith(".")) {
                    values.add(file);
                }
            }
        }
        Collections.sort(values);

        // Put the data into the list
        ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);
        listView.setAdapter(adapter);
    }

    public void createFolder(View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(Explorer.this);
        alert.setTitle("Bikin Folder Baru Bro");
        alert.setMessage("Nama Folder Baru nya Bro");

        final EditText input = new EditText(Explorer.this);
        alert.setView(input);
        alert.setPositiveButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Editable value = input.getText();


                String filename = value.toString();
                if (path.endsWith(File.separator)) {
                    filename = path + filename;
                } else {
                    filename = path + File.separator + filename;
                }

                File folder = new File(filename);
                folder.mkdirs();
                Intent intent = new Intent(Explorer.this, Explorer.class);
                intent.putExtra("path", filename);
                intent.putExtra("sourceImage",sourceImage);
                startActivity(intent);


                Toast.makeText(Explorer.this,"folder dibuat",Toast.LENGTH_LONG).show();

            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alert.show();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_explorer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
