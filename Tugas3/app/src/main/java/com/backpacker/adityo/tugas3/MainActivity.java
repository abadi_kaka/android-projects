package com.backpacker.adityo.tugas3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends Activity {
    ImageView image;
    ImageButton tpict;
    private Uri fileUri;

    public static final int media_foto = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.imageView);
        tpict = (ImageButton) findViewById(R.id.imageButton);
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        if (getIntent().hasExtra("image")) {
            String sourceImage = getIntent().getStringExtra("image");
            Uri uri = Uri.parse(sourceImage);
            image.setImageURI(uri);
            image.setRotation(90);
       }
    }

    public void takePicture(View view){
        Intent in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = Uri.fromFile(getOutputMediaFile(media_foto));
        in.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
        startActivityForResult(in, 101);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
      }

    private File getOutputMediaFile(int type){
        File med = new File(Environment.getExternalStorageDirectory()+"/TugasCamera");
        med.mkdirs();
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File medfile;
        if(type == media_foto){
            medfile = new File(med.getPath() + File.separator + "IMG" + time + ".jpg");
        } else {
            return null;
        }
        return medfile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 101) {
            if (resultCode == RESULT_OK) {
                Intent in = new Intent(MainActivity.this,ImageProcess.class);
                in.putExtra("sourceImage",fileUri.getPath());
                startActivity(in);
            }
        }

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
