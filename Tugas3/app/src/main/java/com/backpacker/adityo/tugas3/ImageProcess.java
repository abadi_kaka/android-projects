package com.backpacker.adityo.tugas3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class ImageProcess extends ActionBarActivity {
    Button cfolder,rename,save,buttGray,buttNor;
    ImageView image;
    TextView iname;
    private String sourceImage = "";
    private String newDir;
    private File fileImage;
    private Bitmap img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_process);
        image = (ImageView)findViewById(R.id.imageView2);
        cfolder = (Button)findViewById(R.id.folder);
        rename = (Button)findViewById(R.id.rename);
        save = (Button)findViewById(R.id.save);
        buttGray = (Button)findViewById(R.id.buttGray);
        buttNor = (Button)findViewById(R.id.buttNor);
        iname = (TextView)findViewById(R.id.imageName);


        if (getIntent().hasExtra("sourceImage")) {
            sourceImage = getIntent().getStringExtra("sourceImage");
            Uri uri = Uri.parse(sourceImage);
            image.setImageURI(uri);
            image.setRotation(90);

            fileImage = new File(uri.getPath());



            BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
            img = drawable.getBitmap();
        }
        if(getIntent().hasExtra("newDir")) {
            newDir = getIntent().getStringExtra("newDir");

            String pet = newDir + File.separator + fileImage.getName();
            File fileRename = new File(pet);
            fileImage.renameTo(fileRename);
            fileImage = new File(fileRename.getPath());
            sourceImage = fileImage.getPath();
            Toast.makeText(ImageProcess.this, "directory ganti",Toast.LENGTH_LONG).show();

        }
        iname.setText(sourceImage);

        cfolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ImageProcess.this,FileExplorer.class);
                in.putExtra("sourceImage",sourceImage);
                startActivity(in);
            }
        });

        buttGray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap grayscaleBitmap = Bitmap.createBitmap(
                        img.getWidth(), img.getHeight(),
                        Bitmap.Config.RGB_565);

                Canvas c = new Canvas(grayscaleBitmap);
                Paint p = new Paint();
                ColorMatrix cm = new ColorMatrix();

                cm.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(cm);
                p.setColorFilter(filter);
                c.drawBitmap(img, 0, 0, p);
                image.setImageBitmap(grayscaleBitmap);
            }
        });

        buttNor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setImageBitmap(img);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(fileImage);
                    BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
                    img = drawable.getBitmap();
                    img.compress(Bitmap.CompressFormat.JPEG, 100, out);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent in = new Intent(ImageProcess.this,MainActivity.class);
                in.putExtra("image",fileImage.getPath());
                startActivity(in);
                finish();
            }
        });

    }



    public void renameImage(View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(ImageProcess.this);
        alert.setTitle("Rename Image");
        alert.setMessage("Nama Image");

        final EditText input = new EditText(ImageProcess.this);
        alert.setView(input);
        alert.setPositiveButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Editable value = input.getText();
                String pet = fileImage.getParent() + File.separator + value.toString() + ".jpg";
                //String pet = Environment.getExternalStorageDirectory() + File.separator + value.toString() +".jpg";
                File fileRename = new File(pet);
                fileImage.renameTo(fileRename);
                fileImage = new File(fileRename.getPath());
                iname.setText(fileImage.getName());
                sourceImage = fileImage.getPath();
                Toast.makeText(ImageProcess.this, "nama diganti", Toast.LENGTH_LONG).show();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_process, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
