package com.example.macbook.gdgdevfest.presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.macbook.gdgdevfest.R;

public class AddActivity extends AppCompatActivity implements AddView {

    private EditText etInputLink;
    private Button btnSave;
    private AddPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        etInputLink = (EditText) findViewById(R.id.etAddLink);
        btnSave = (Button) findViewById(R.id.btnAddSave);

        presenter = new AddPresenter(this);

        Intent intent = getIntent();
        String action  = intent.getAction();
        String type = intent.getType();

        if(Intent.ACTION_SEND.equals(action) && type != null){
            if("text/plain".equals(type)){
                handleShareabelLink(intent);
            }
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.saveLink(etInputLink.getText().toString());
            }
        });
    }

    private void handleShareabelLink(Intent intent){
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

        if(sharedText !=  null){
            if(!TextUtils.isEmpty(etInputLink.getText().toString())){
                etInputLink.setText("");
            }
            etInputLink.setText(sharedText);
        }
    }
}
