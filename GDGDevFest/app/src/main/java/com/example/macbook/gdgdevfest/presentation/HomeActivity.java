package com.example.macbook.gdgdevfest.presentation;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.macbook.gdgdevfest.R;

public class HomeActivity extends AppCompatActivity {

    private FloatingActionButton fabHome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fabHome = (FloatingActionButton) findViewById(R.id.fabHome);

        fabHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 11/5/16 Navigate add activity
                Intent in = new Intent(getApplicationContext(),AddActivity.class);
                startActivity(in);
            }
        });
    }
}
