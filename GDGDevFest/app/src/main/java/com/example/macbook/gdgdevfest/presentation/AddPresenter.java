package com.example.macbook.gdgdevfest.presentation;

import android.text.TextUtils;
import android.util.Log;

import com.example.macbook.gdgdevfest.data.entity.Article;
import com.google.firebase.database.DatabaseReference;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reactivestreams.Subscriber;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.operators.observable.ObservableAll;
import rx.schedulers.Schedulers;

/**
 * Created by macbook on 11/5/16.
 */

public class AddPresenter {
    private AddView view;
    private static final String TAG = "TAG";
   /private DatabaseReference databaseReference;
//    private FirebaseAuth firebaseAuth;
//    private FirebaseUser firebaseUser;
//    private CompositeSubscription compositeSubscription = new CompositeSubscription();


    public AddPresenter(AddView view) {
        this.view = view;
//        databaseReference = FirebaseDatabase.getInstance().getReference();
//        firebaseAuth = FirebaseAuth.getInstance();
//        firebaseUser = firebaseAuth.getCurrentUser();
    }

//    public void onDetachView() {
//        if (compositeSubscription != null) {
//            compositeSubscription.unsubscribe();
//        }
//    }
//
//    public void onAttachView() {
//        compositeSubscription = new CompositeSubscription();
//    }


    public void saveLink(String s) {

        scrapingURL( s ).subscribeOn(Schedulers.io())
                .subscribe(new rx.Subscriber<Article>() {
                    @Override
                    public void onCompleted() {
                        view.hideLoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("TAG", "onError: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(Article article) {
                            view.hideLoading();
                            String key = databaseReference.child("articles").push().getKey();
                            Map<String, Object> postValues = article.toMap();
                            Map<String, Object> childUpdates = new HashMap<String, Object>();

                            childUpdates.put("/articles/" + key, postValues);
                            databaseReference.updateChildren(childUpdates);
                            view.saveSuccess();
                    }
                });
    }

    public rx.Observable<Article> scrapingURL(final String URL) {
        return Observable.create(new rx.Observable.OnSubscribe<Article>() {
            @Override
            public void call(rx.Subscriber<? super Article> subscriber) {
                try {
                    Document doc = Jsoup.connect(URL).get();

                    String redirectedUrl = null;
                    Elements meta = doc.select("html head meta");
                    if (meta.attr("http-equiv").contains("REFRESH")) {
                        redirectedUrl = meta.attr("content").split("=")[1];
                    } else {
                        if (doc.toString().contains("window.location.href")) {
                            meta = doc.select("script");
                            for (Element script : meta) {
                                String s = script.data();
                                if (!s.isEmpty() && s.startsWith("window.location.href")) {
                                    int start = s.indexOf("=");
                                    int end = s.indexOf(";");
                                    if (start > 0 && end > start) {
                                        s = s.substring(start + 1, end);
                                        s = s.replace("'", "").replace("\"", "");
                                        redirectedUrl = s.trim();
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    String title = getMetaTag(doc, "og:title");
                    String img = getMetaTag(doc, "og:image");
                    //Timber.d("call() : title %s", title);
                    //Timber.d("call() : img %s", img);

                    if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(img)) {

                        subscriber.onNext(new Article.Builder().uId(firebaseUser.getUid())
                                .title(title)
                                .image(img)
                                .url(URL)
                                .build());
                        subscriber.onCompleted();
                    } else {
                        subscriber.onError(new Throwable("Empty title or img"));
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }

        });
    }

    private String getMetaTag(Document document, String attr) {
        Elements elements = document.select("meta[name=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) return s;
        }
        elements = document.select("meta[property=" + attr + "]");
        for (Element element : elements) {
            final String s = element.attr("content");
            if (s != null) return s;
        }
        return null;
    }
}

