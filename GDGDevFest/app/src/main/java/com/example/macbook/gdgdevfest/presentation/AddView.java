package com.example.macbook.gdgdevfest.presentation;

/**
 * Created by macbook on 11/5/16.
 */

public interface AddView {

    void hideLoading();
    void saveSuccess();

}